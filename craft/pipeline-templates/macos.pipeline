// Sometimes we need to include additional parameters to Craft which are specific to the project
def craftProjectParams = ""

// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfig = "binary-factory-tooling/craft/configs/master/CraftBinaryCache.ini"

// Determine if we need to build a specific version of this project
if( craftTarget != '' ) {
	// If we have a specified version (Craft target) then we need to pass this along to Craft
	craftProjectParams = "--target ${craftTarget}"
}

// Request a node to be allocated to us
node( "macOS" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// Make sure the Git checkouts are up to date
			sh """
				/usr/local/bin/python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository https://invent.kde.org/packaging/craftmaster.git --into ~/Craft/BinaryFactory/
				/usr/local/bin/python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository https://invent.kde.org/sysadmin/binary-factory-tooling.git --into ~/Craft/BinaryFactory/
			"""

			// Update Craft itself
			sh """
				cd ~/Craft/BinaryFactory/
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c -i craft
			"""
		}

		stage('Cleaning Up Prior Builds') {
			// Cleanup our workspace
			deleteDir()

			// Ensure we have nothing left behind in the packaging workspace used by Craft
			sh """
				cd ~/Craft/BinaryFactory/
				packageDir=\$(/usr/local/bin/python3 "craftmaster/Craftmaster.py" --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} -q --get "packageDestinationDir()" virtual/base)
				rm -rf "\$packageDir"
			"""

			// Make sure the build environment for this application is clean
			sh """
				cd ~/Craft/BinaryFactory/
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --unmerge ${craftRebuildBlueprint}
			"""
		}

		stage('Installing Dependencies') {
			// Ask Craftmaster to ensure all the dependencies are installed for this application we are going to be building
			sh """
				cd ~/Craft/BinaryFactory/
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --install-deps ${craftBuildBlueprint}
			"""
		}

		stage('Building') {
			// Actually build the application now
			sh """
				cd ~/Craft/BinaryFactory/
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --no-cache ${craftProjectParams} ${craftRebuildBlueprint}
			"""
		}

		stage('Packaging') {
			// Now generate an installer for it
			sh """
				cd ~/Craft/BinaryFactory/
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --package ${craftBuildBlueprint}
			"""

			// Now copy it to the Jenkins workspace so it can be grabbed from there
			sh """
				cd ~/Craft/BinaryFactory/
				packageDir=\$(/usr/local/bin/python3 "craftmaster/Craftmaster.py" --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} -q --get "packageDestinationDir()" virtual/base)

				cp -vf \$packageDir/* \$WORKSPACE/
				rm -rf "\$packageDir"
                /usr/local/bin/python3 craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --clean-unused
			"""
		}

		stage('Capturing Package') {
			// Then we ask Jenkins to capture the generated installers as an artifact
			archiveArtifacts artifacts: '*.dmg, *.sha256', onlyIfSuccessful: true
		}
	}

	// As the Mac Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()
}
}

