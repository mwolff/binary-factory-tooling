// Ask for parameters we will need later on
def buildParameters = input(
	message: 'Which version of Krita is being built?',
	ok: 'Begin Build',
	parameters: [
		string(defaultValue: '', description: '', name: 'Version', trim: true)
	]
)

// Pull the version we've been given out to a separate variable
// We need to do this otherwise Jenkins will throw it's toys and claim we are violating a security sandbox which will expose our instance to vulnerabilites
// The Jenkins Security Sandbox is IMO broken and faulty in this regard
def buildVersion = buildParameters ?: ''

// Request a node to be allocated to us
node( "WindowsMingw" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Now we download the release tarball, unpack it and rename the directory to something more convenient to use everywhere else
			bat """
				set PATH=C:/MingW/i686-7.3.0-posix-dwarf-rt_v5-rev0/mingw32/bin;C:/PROGRA~2/Python38-32/;C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/;%PATH%
				set CA_CERT_BUNDLE=C:/Craft/Krita/windows-msvc2019_64-cl/etc/cacert.pem

				wget --ca-certificate %CA_CERT_BUNDLE% "https://origin.files.kde.org/krita/.release/${buildVersion}/krita-${buildVersion}.tar.gz"

				7za x "%WORKSPACE%/krita-${buildVersion}.tar.gz"
				7za x "%WORKSPACE%/krita-${buildVersion}.tar"

				ren krita-${buildVersion} krita
			"""
		}

		// Now retrieve the dependency artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-deps.zip', projectName: 'Krita_Release_Windows32_Dependency_Build'

			// Now we unpack them
			bat """
				mkdir C:\\Packaging\\KritaWS\\
				cd C:\\Packaging\\KritaWS\\
				C:\\Craft\\Krita\\windows-msvc2019_64-cl\\dev-utils\\bin\\7za.exe x "%WORKSPACE%\\krita-deps.zip"
			"""
		}

		// Now we can build Krita
		stage('Building Krita') {
			// Before we can do this we need to setup some environment variables to help guide the Krita scripts around
			// In particular it needs:
			// PATH set to guide it to CMake and to allow it to find Mingw (Git and Python are already in PATH as standard convention for these builders)
			// SEVENZIP_EXE set to guide it to 7-Zip's console utility. It's detection support doesn't work as our binary is named 7za.exe instead of the apparently standard 7z.exe
			// Then we can run it
			// We also set SIGNTOOL_SIGN_FLAGS which is used by the package signing script when invoking signtool.exe
			bat """
				set PATH=C:/MingW/i686-7.3.0-posix-dwarf-rt_v5-rev0/mingw32/bin;C:/PROGRA~2/Python38-32/;C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/;%PATH%
				set SEVENZIP_EXE=C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/7za.exe
				set GETTEXT_SEARCH_PATH=C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/PROGRA~2/Python38-32/


				cmd /c krita\\build-tools\\windows\\build.cmd --no-interactive --skip-deps --src-dir "%WORKSPACE%\\krita" --download-dir "%WORKSPACE%\\downloads" --krita-build-dir "%WORKSPACE%\\build" --deps-install-dir "C:\\Packaging\\KritaWS\\deps-install" --krita-install-dir "C:\\Packaging\\KritaWS\\krita-install" 
				
				if errorlevel 1 exit /b %errorlevel%

				set SIGNTOOL_SIGN_FLAGS=/v /n "K Desktop Environment e.V." /tr "http://timestamp.digicert.com" /td SHA256 /fd SHA256

				cmd /c krita\\packaging\\windows\\package-complete.cmd --no-interactive --src-dir "%WORKSPACE%\\krita" --deps-install-dir "C:\\Packaging\\KritaWS\\deps-install" --krita-install-dir "C:\\Packaging\\KritaWS\\krita-install"  --pre-zip-hook "%WORKSPACE%\\krita\\packaging\\windows\\sign-package.cmd" --package-name "krita-x86-${buildVersion}"
				
				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		stage('Generating Installer') {
			// Let's build an installer!
			bat """
				set PATH=C:/MingW/i686-7.3.0-posix-dwarf-rt_v5-rev0/mingw32/bin;C:/PROGRA~2/Python38-32/;C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/;%PATH%
				set PACKAGE_NAME=krita-x86-${buildVersion}

				cd C:\\Packaging\\KritaWS
				mkdir installer
				cd installer
				cmake -DREMOVE_DEBUG=ON -DOUTPUT_FILEPATH="%WORKSPACE%\\%PACKAGE_NAME%-setup.exe" -DKRITA_PACKAGE_ROOT="%WORKSPACE%\\%PACKAGE_NAME%" -P "C:\\Packaging\\KritaWS\\krita-install\\MakeinstallerNsis.cmake"
				if errorlevel 1 exit /b 1

				call "C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvars64.bat"
				set SIGNTOOL_SIGN_FLAGS=/v /n "K Desktop Environment e.V." /tr "http://timestamp.digicert.com" /td SHA256 /fd SHA256

				signtool sign %SIGNTOOL_SIGN_FLAGS% "%WORKSPACE%\\%PACKAGE_NAME%-setup.exe"
			"""
		}

		// Now we capture them for use 
		stage('Capturing Build') {
			// To do this we just ask Jenkins to capture the zip file as an artifact
			archiveArtifacts artifacts: 'krita-x86-*.zip, krita-x86-*-setup.exe', onlyIfSuccessful: true
		}
	}

	// As the Windows Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()

	// Same goes for the stable workspace that we use
	bat """
	   rmdir /s /q C:\\Packaging\\KritaWS\\
	"""
}
}
