// First we'll update Builder Iota....
node( "Windows Builder Iota" ) {
	// We want Timestamps on everything
	timestamps {
		// First we want to make sure Craft is ready to go
		stage('Update Tooling') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/binary-factory-tooling']]
			]

			// Make sure that Craftmaster is up to date
			bat """
				python "%WORKSPACE%\\bf-tooling\\craft\\checkout-repository.py" --repository git://anongit.kde.org/craftmaster --into "C:/Craft/Krita/" --branch master
			"""
		}

		// Now that is done, build out the two targets we need
		stage('Update Utilities') {
			bat """
				cd C:\\Craft\\Krita\\

				python craftmaster/Craftmaster.py --config "%WORKSPACE%/bf-tooling/craft/configs/stable/CraftBinaryCache.ini" --target windows-msvc2019_64-cl -c -i --no-cache craft
				if errorlevel 1 exit /b %errorlevel%

				python craftmaster/Craftmaster.py --config "%WORKSPACE%/bf-tooling/craft/configs/stable/CraftBinaryCache.ini" --target windows-msvc2019_64-cl -c -i nsis
				if errorlevel 1 exit /b %errorlevel%

				python craftmaster/Craftmaster.py --config "%WORKSPACE%/bf-tooling/craft/configs/stable/CraftBinaryCache.ini" --target=windows-msvc2019_64-cl -c --list-file %WORKSPACE%/bf-tooling/custom-jobs/krita/utilities-packages.list
				if errorlevel 1 exit /b %errorlevel%
			"""
		}
	}
}

// Then Builder Kappa....
node( "Windows Builder Kappa" ) {
	// We want Timestamps on everything
	timestamps {
		// First we want to make sure Craft is ready to go
		stage('Update Tooling') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/binary-factory-tooling']]
			]

			// Make sure that Craftmaster is up to date
			bat """
				python "%WORKSPACE%\\bf-tooling\\craft\\checkout-repository.py" --repository git://anongit.kde.org/craftmaster --into "C:/Craft/Krita/" --branch master
			"""
		}

		// Now that is done, build out the two targets we need
		stage('Update Utilities') {
			bat """
				cd C:\\Craft\\Krita\\

				python craftmaster/Craftmaster.py --config "%WORKSPACE%/bf-tooling/craft/configs/stable/CraftBinaryCache.ini" --target windows-msvc2019_64-cl -c -i --no-cache craft
				if errorlevel 1 exit /b %errorlevel%

				python craftmaster/Craftmaster.py --config "%WORKSPACE%/bf-tooling/craft/configs/stable/CraftBinaryCache.ini" --target windows-msvc2019_64-cl -c -i nsis
				if errorlevel 1 exit /b %errorlevel%

				python craftmaster/Craftmaster.py --config "%WORKSPACE%/bf-tooling/craft/configs/stable/CraftBinaryCache.ini" --target=windows-msvc2019_64-cl -c --list-file %WORKSPACE%/bf-tooling/custom-jobs/krita/utilities-packages.list
				if errorlevel 1 exit /b %errorlevel%
			"""
		}
	}
}

// Now the Mac builder
node( "macOS Builder 1" ) {
	// We want Timestamps on everything
	timestamps {
		// First we want to make sure Craft is ready to go
		stage('Update Tooling') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/binary-factory-tooling']]
			]

			// Make sure that Craftmaster is up to date
			sh """
				/usr/local/bin/python3 \$WORKSPACE/bf-tooling/craft/checkout-repository.py --repository "git://anongit.kde.org/craftmaster" --into \$HOME/Craft/Krita/ --branch master
			"""
		}

		// Now that is done, build out the two targets we need
		stage('Update Utilities') {
			sh """
				set -e
				cd \$HOME/Craft/Krita/

				/usr/local/bin/python3 craftmaster/Craftmaster.py --config \$WORKSPACE/bf-tooling/craft/configs/stable/CraftBinaryCache.ini --target macos-64-clang -c -i --no-cache craft

				/usr/local/bin/python3 craftmaster/Craftmaster.py --config \$WORKSPACE/bf-tooling/craft/configs/stable/CraftBinaryCache.ini --target macos-64-clang -c --list-file \$WORKSPACE/bf-tooling/custom-jobs/krita/utilities-packages.list
			"""
		}
	}
}
