// Request a node to be allocated to us
node( "SUSEWeb" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
        // First Thing: Checkout Sources
        stage('Checkout Sources') {
            // Make sure we have a clean slate to begin with
            deleteDir()
            // Appstream Metadata Extractor
            checkout changelog: true, poll: true, scm: [
                $class: 'GitSCM',
                branches: [[name: 'master']],
                extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'extractor/']],
                userRemoteConfigs: [[url: "https://invent.kde.org/websites/kde-org-applications-extractor.git"]]
            ]
            // Applications subsite code
            checkout changelog: true, poll: true, scm: [
                $class: 'GitSCM',
                branches: [[name: 'master']],
                extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'site/']],
                userRemoteConfigs: [[url: "https://invent.kde.org/websites/apps-kde-org.git"]]
            ]
        }

        // Let's build website now
        stage('Website build') {
            sh """
                export LANG=en_US.UTF-8

                cd extractor/
                gem install --user -g Gemfile

                ruby appstream.rb
                ruby appstream_mkindex.rb

                cd ../site
                composer install --optimize-autoloader
                
                cd ..
                mv extractor/appdata-extensions/ site/
                mv appdata/ thumbnails/ index.json site/
                mv app-icons/* site/app-icons
            """
        }

        // Deploy the website!
        stage('Publishing Website') {
            sh """
                rsync -Hrlpvc --delete -e "ssh -i $HOME/WebsitePublishing/website-upload.key" site/ sitedeployer@nicoda.kde.org:${deploypath}
                ssh -i $HOME/WebsitePublishing/website-upload.key  sitedeployer@nicoda.kde.org "cd ${deploypath} && APP_ENV=prod APP_DEBUG=0 bin/console cache:warmup"
            """
        }
    }
}
}
